/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package almacen;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 *
 * @author ppinheirofranciscogomez
 */
public class Producto {
    private String nombre;
    private int codigo;
    private float precio;
    Map <String,Producto> almacen = new TreeMap();
    
    public Producto(){
        
    }
    
    public Producto(String nombre, float precio){
        this.nombre=nombre;
        this.precio=precio;
    }
    
    public void añadirProducto(Producto producto1){
        Scanner dato=new Scanner(System.in);        
        System.out.println("Introduce el nombre del producto: ");
        producto1.setNombre(dato.next());
        System.out.println("Introduce el precio del producto: ");
        producto1.setPrecio(dato.nextFloat());
        System.out.println("Introduce el código del producto: ");
        setCodigo(dato.nextInt());
    }

    public void verProducto(Producto producto1){
        for(Map.Entry entrada:almacen.entrySet()){
            System.out.println("Producto : "+entrada.getKey()+","+entrada.getValue());
    }  
    
}

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the precio
     */
    public float getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }
}



